/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.page;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PageTest extends AbstractDemoTest {

	public void test() {
		test1();
	}

	private void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建demo页面");
			conn = JdbcTransactions.getConnection(ds1);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.category");
			pstmt.setString(2, "商品类型");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page1.xml")));
			pstmt.setString(5, page1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods");
			pstmt.setString(2, "普通页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page2.xml")));
			pstmt.setString(5, page2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d1");
			pstmt.setString(2, "普通列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page3.xml")));
			pstmt.setString(5, page3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d2");
			pstmt.setString(2, "多层列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page4.xml")));
			pstmt.setString(5, page4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d3");
			pstmt.setString(2, "自定义查询");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page5.xml")));
			pstmt.setString(5, page5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d4");
			pstmt.setString(2, "多层列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page6.xml")));
			pstmt.setString(5, page6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d5");
			pstmt.setString(2, "单元格计算、风格及事件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page7 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page7.xml")));
			pstmt.setString(5, page7);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.goods.d6");
			pstmt.setString(2, "列表与页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page8 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page8.xml")));
			pstmt.setString(5, page8);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.customer");
			pstmt.setString(2, "普通页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page9 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer.xml")));
			pstmt.setString(5, page9);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.customer.d1");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page10 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_list.xml")));
			pstmt.setString(5, page10);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.customer.d2");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page11 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_order.xml")));
			pstmt.setString(5, page11);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.cust_order");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page12 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("cust_order.xml")));
			pstmt.setString(5, page12);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.order_detail");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page13 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("order_detail.xml")));
			pstmt.setString(5, page13);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.customer.d3");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page14 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_list_1.xml")));
			pstmt.setString(5, page14);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.customer.d4");
			pstmt.setString(2, "主副表页面-1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page15 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer_order_1.xml")));
			pstmt.setString(5, page15);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.pie3d.1");
			pstmt.setString(2, "图表与主表数据集相同");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page16 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("pie3d_1.xml")));
			pstmt.setString(5, page16);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.pie3d.2");
			pstmt.setString(2, "图表与主表数据集不同");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page17 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("pie3d_2.xml")));
			pstmt.setString(5, page17);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.crosstab.1");
			pstmt.setString(2, "交叉表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page18 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("crosstab_1.xml")));
			pstmt.setString(5, page18);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.subreport.1");
			pstmt.setString(2, "内嵌子表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page19 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("subreport_1.xml")));
			pstmt.setString(5, page19);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.common.report");
			pstmt.setString(2, "通用报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page20 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("common_report.xml")));
			pstmt.setString(5, page20);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.report.1");
			pstmt.setString(2, "列表报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page21 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_report_1.xml")));
			pstmt.setString(5, page21);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.report.2");
			pstmt.setString(2, "主副表页面及报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page22 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_report_2.xml")));
			pstmt.setString(5, page22);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.custom.report.1");
			pstmt.setString(2, "自定义报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String page23 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("custom_report_1.xml")));
			pstmt.setString(5, page23);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.user.leave");
			pstmt.setString(2, "用户请假");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String user_leave_page = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream("user_leave.xml")));
			pstmt.setString(5, user_leave_page);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.common.datalist");
			pstmt.setString(2, "公共清单页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String common_datalist = Streams.readAndClose(Streams
					.utf8r(getClass()
							.getResourceAsStream("common_datalist.xml")));
			pstmt.setString(5, common_datalist);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.bpm.view");
			pstmt.setString(2, "工作流流程图");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String bpm_view = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_view.xml")));
			pstmt.setString(5, bpm_view);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.bpm.test");
			pstmt.setString(2, "流程测试案例");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String bpm_test = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_test.xml")));
			pstmt.setString(5, bpm_test);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.charts.1");
			pstmt.setString(2, "图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String charts1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_1.xml")));
			pstmt.setString(5, charts1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.charts.2");
			pstmt.setString(2, "图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String charts2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_2.xml")));
			pstmt.setString(5, charts2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.charts.3");
			pstmt.setString(2, "图表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String charts3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("charts_3.xml")));
			pstmt.setString(5, charts3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.action.1");
			pstmt.setString(2, "页面操作");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String pageaction = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page_action.xml")));
			pstmt.setString(5, pageaction);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.complex.1");
			pstmt.setString(2, "多功能页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String pagecomplex = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("page_complex.xml")));
			pstmt.setString(5, pagecomplex);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.datalist.load");
			pstmt.setString(2, "数据来源于load函数");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String pageload = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist_load.xml")));
			pstmt.setString(5, pageload);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.1");
			pstmt.setString(2, "普通分组");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree1.xml")));
			pstmt.setString(5, tree1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.2");
			pstmt.setString(2, "分组编辑");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree2.xml")));
			pstmt.setString(5, tree2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.3");
			pstmt.setString(2, "主副编辑1");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree3.xml")));
			pstmt.setString(5, tree3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.4");
			pstmt.setString(2, "主副编辑2");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree4.xml")));
			pstmt.setString(5, tree4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.5");
			pstmt.setString(2, "上下层关系");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree5.xml")));
			pstmt.setString(5, tree5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.tree.6");
			pstmt.setString(2, "可编辑的上下层关系");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String tree6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree6.xml")));
			pstmt.setString(5, tree6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.group.1");
			pstmt.setString(2, "清单报表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String group1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("group1.xml")));
			pstmt.setString(5, group1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.component.1");
			pstmt.setString(2, "扩展控件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String component1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("component.xml")));
			pstmt.setString(5, component1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.component.2");
			pstmt.setString(2, "扩展控件h5");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String component2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("canvas.xml")));
			pstmt.setString(5, component2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.component.3");
			pstmt.setString(2, "扩展控件ecotree");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String component3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("ecotree.xml")));
			pstmt.setString(5, component3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.page.list.batch");
			pstmt.setString(2, "批量更新");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.PAGE.getName());
			String listbatch = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("listbatch.xml")));
			pstmt.setString(5, listbatch);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("新建demo页面成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("PageTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
