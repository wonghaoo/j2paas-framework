/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.h5;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.cfg.IdGenerator;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.im.SendRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.h5.MsnVo;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.messages.vos.h5.MessageVo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SendCmd extends AbstractCommand<SendRequestMessage> {

	/**
	 * @param req
	 */
	public SendCmd(SendRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		MsnVo mv = req.getBody();
		IdGenerator idg = cc.getIdGenerator();
		BizDao dao = cc.getBizDao();
		List<FieldDo> params = new ArrayList<FieldDo>();
		Long id = idg.getNextId("sys_chat_info");
		params.add(new FieldDo(FieldType.LONG, id));
		params.add(new FieldDo(FieldType.VARCHAR, mv.getText()));
		try {
			cc.beginTx();
			dao.update(cc.getUser(),
					"insert into sys_chat_info(msgid,msgtext) values(?,?)",
					params, false);
			Date now = new Date();
			for (String user : mv.getToUsers()) {
				params.clear();
				params.add(new FieldDo(FieldType.LONG, id));
				params.add(new FieldDo(FieldType.VARCHAR, user));
				params.add(new FieldDo(FieldType.VARCHAR, cc.getUser().getId()));
				params.add(new FieldDo(FieldType.INT, mv.getGroupId()));
				params.add(new FieldDo(FieldType.INT, user.equals(cc.getUser()
						.getId()) ? MessageVo.STATE_READED
						: MessageVo.STATE_UNREAD));
				params.add(new FieldDo(FieldType.DATETIME, now));
				dao.update(
						cc.getUser(),
						"insert into sys_chat_detail_info(msgid,touser,fromuser,groupid,msgstatus,msgtime) values(?,?,?,?,?,?)",
						params, false);
			}
			cc.commitTx();
		} catch (Exception ex) {
			cc.rollbackTx();
			if (ex instanceof EasyPlatformWithLabelKeyException)
				throw (EasyPlatformWithLabelKeyException) ex;
			return new SimpleResponseMessage("e301", I18N.getLabel(
					"page.msn.error", ex.getMessage()));
		} finally {
			cc.closeTx();
		}
		return new SimpleResponseMessage(id);
	}

}
