/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.addon;

import cn.easyplatform.dao.Page;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.addon.SearchResultVo;
import cn.easyplatform.messages.vos.addon.SearchVo;
import cn.easyplatform.type.IResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SearchEntityCmd extends AbstractCommand<SimpleRequestMessage> {

    private final static Logger log = LoggerFactory.getLogger(SearchEntityCmd.class);

    /**
     * @param req
     */
    public SearchEntityCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (cc.getProjectService() == null)
            return new SimpleResponseMessage("E099",
                    I18N.getLabel("easyplatform.not.allow.cmd"));
        SearchVo vo = (SearchVo) req.getBody();
        Page page = null;
        if (vo.getPageSize() > 0 && vo.getStartPageNo() > 0) {
            page = new Page(vo.getPageSize());
            page.setGetTotal(vo.isGetTotal());
            page.setPageNo(vo.getStartPageNo());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT entityId,name,desp,type,subType FROM ").append(cc.getProjectService().getEntity().getEntityTableName()).append(" WHERE 1=1");
        List<Object> params = new ArrayList<>();
        if (!Strings.isBlank(vo.getId())) {
            sb.append(" AND entityId LIKE ?");
            params.add("%" + vo.getId() + "%");
        }
        if (!Strings.isBlank(vo.getName())) {
            sb.append(" AND name LIKE ?");
            params.add("%" + vo.getName() + "%");
        }
        if (!Strings.isBlank(vo.getType())) {
            sb.append(" AND type = ?");
            params.add(vo.getType());
        }
        if (!Strings.isBlank(vo.getSubType())) {
            sb.append(" AND subType = ?");
            params.add(vo.getSubType());
        }
        if (log.isDebugEnabled())
            log.debug("sql->{},{}", sb, params);
        List<String[]> data = cc.getEntityDao().getEntities(sb.toString(), page, params.toArray());
        if (vo.isGetTotal())
            return new SimpleResponseMessage(new SearchResultVo(page.getTotalCount(), data));
        return new SimpleResponseMessage(data);
    }
}
