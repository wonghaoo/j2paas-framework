/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface UserType {

    /**
     * 用户类型：API用户，不能登陆
     */
    public final static int TYPE_API = -1;

    /**
     * 用户类型：独占式，只允计在一个设备上登陆
     */
    public final static int TYPE_EXCLUSIVE = 0;

    /**
     * 用户类型：共享式，可以在任何设备同时登陆
     */
    public final static int TYPE_SHARE = 1;

    /**
     * 用户类型：独占式，允许在其它设备登陆，如果用户已在线上则中断用户
     */
    public final static int TYPE_ONLY = 2;

    /**
     * 管理用户
     */
    public final static int TYPE_ADMIN = 3;

    /**
     * 超级用户，管理所有云端项目
     */
    public final static int TYPE_SUPER = 8;

    /**
     * 第3方之虚拟用户
     */
    public final static int TYPE_OAUTH = 9;

    /**
     * 匿名用户
     */
    public final static int TYPE_ANONYMOUS = 10;
}
