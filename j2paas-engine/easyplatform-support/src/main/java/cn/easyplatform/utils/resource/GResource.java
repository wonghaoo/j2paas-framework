/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils.resource;

import cn.easyplatform.lang.Streams;

import java.io.InputStream;
import java.io.Reader;

public abstract class GResource {

    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (obj instanceof GResource)
            return this.name.equals(((GResource) obj).name);
        return false;
    }

    /**
     * 使用完毕后,务必关闭
     */
    public abstract InputStream getInputStream();

    public String getName() {
        return name;
    }

    /**
     * 使用完毕后,务必关闭
     */
    public Reader getReader() {
        return Streams.utf8r(getInputStream());
    }

    public int hashCode() {
        return null == name ? "NULL".hashCode() : name.hashCode();
    }

    public GResource setName(String name) {
        this.name = name;
        return this;
    }

    public String toString() {
        return String.format("GResource[%s]", name);
    }

    protected String name;

}
