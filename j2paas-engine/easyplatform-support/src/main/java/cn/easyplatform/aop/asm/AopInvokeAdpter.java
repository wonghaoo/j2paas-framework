/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.asm;

import cn.easyplatform.org.objectweb.asm.Label;
import cn.easyplatform.org.objectweb.asm.MethodVisitor;
import cn.easyplatform.org.objectweb.asm.Type;

import java.lang.reflect.Method;

/**
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 * 
 */
class AopInvokeAdpter extends AopMethodAdapter {

	Method[] methodArray;

	AopInvokeAdpter(Method[] methodArray, MethodVisitor mv, int access,
			String methodName, String desc, int methodIndex, String myName,
			String enhancedSuperName) {
		super(mv, access, methodName, desc, methodIndex, myName,
				enhancedSuperName);
		this.methodArray = methodArray;
	}

	void visitCode() {
		mv.visitCode();

		for (int i = 0; i < methodArray.length; i++) {
			Method method = methodArray[i];
			mv.visitVarInsn(ILOAD, 1);
			visitX(i);
			Label l0 = new Label();
			mv.visitJumpInsn(IF_ICMPNE, l0);
			mv.visitVarInsn(ALOAD, 0);
			Type[] args = Type.getArgumentTypes(method);
			for (int j = 0; j < args.length; j++) {
				mv.visitVarInsn(ALOAD, 2);
				visitX(j);
				mv.visitInsn(AALOAD);
				returnType = args[j];
				AsmHelper.checkCast(returnType, mv);
			}
			mv.visitMethodInsn(INVOKESPECIAL, enhancedSuperName,
					method.getName(), Type.getMethodDescriptor(method));
			{
				returnType = Type.getReturnType(method);
				if (returnType.equals(Type.VOID_TYPE))
					mv.visitInsn(ACONST_NULL);
				else if (returnType.getOpcode(IRETURN) != ARETURN)
					AsmHelper.packagePrivateData(returnType, mv);
				mv.visitInsn(ARETURN);
			}
			mv.visitLabel(l0);
		}

		mv.visitInsn(ACONST_NULL);
		mv.visitInsn(ARETURN);
		mv.visitMaxs(4, 3);
		mv.visitEnd();
	}

}
