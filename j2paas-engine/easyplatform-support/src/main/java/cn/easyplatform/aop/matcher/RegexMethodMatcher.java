/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.matcher;

import cn.easyplatform.aop.MethodMatcher;
import cn.easyplatform.lang.Maths;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.regex.Pattern;

import static java.lang.reflect.Modifier.TRANSIENT;

public class RegexMethodMatcher implements MethodMatcher {

	private Pattern active;
	private Pattern ignore;
	private int mods;

	public RegexMethodMatcher(String active) {
		this(active, null);
	}

	public RegexMethodMatcher(String active, String ignore) {
		this(active, ignore, Modifier.PUBLIC | Modifier.PROTECTED);
	}

	public RegexMethodMatcher(String active, String ignore, int mods) {
		if (active != null)
			this.active = Pattern.compile(active);
		if (ignore != null)
			this.ignore = Pattern.compile(ignore);
		this.mods = mods;
	}

	public boolean match(Method method) {
		int mod = method.getModifiers();
		String name = method.getName();
		if (null != ignore)
			if (ignore.matcher(name).find())
				return false;
		if (null != active)
			if (!active.matcher(name).find())
				return false;
		if (mods <= 0)
			return true;

		if (mod == 0)
			mod |= TRANSIENT;

		return Maths.isMask(mod, mods);
	}

}
