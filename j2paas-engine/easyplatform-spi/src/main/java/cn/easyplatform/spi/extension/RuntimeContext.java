/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.extension;

/**
 * 逻辑处理环境
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface RuntimeContext {

    /**
     * 给栏位或变量赋值
     *
     * @param name
     * @param value
     */
    void setValue(String name, Object value);

    /**
     * 取栏位或变量的值
     *
     * @param name
     * @return
     */
    Object getValue(String name);

    /**
     * 输出到客户端控制台
     *
     * @param msg
     */
    void debug(Object msg);

    /**
     * 输出到log
     *
     * @param obj
     */
    void log(Object obj);

    /**
     * 错误时输出
     *
     * @param obj
     * @param e
     */
    void error(Object obj, Exception e);
}
