/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.extension;

import java.util.Date;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ApplicationService {

    /**
     * 服务id
     *
     * @return
     */
    String getId();

    /**
     * 服务名称
     *
     * @return
     */
    String getName();

    /**
     * 服务描述
     *
     * @return
     */
    String getDescription();

    /**
     * 启动服务
     */
    void start();

    /**
     * 停止服务
     */
    void stop();

    /**
     * 获取运行时信息
     *
     * @return
     */
    Map<String, Object> getRuntimeInfo();

    /**
     * 服务状态
     *
     * @return
     */
    int getState();

    /**
     * 设置服务状态
     *
     * @param state
     */
    void setState(int state);

    /**
     * 启动时间
     *
     * @return
     */
    Date getStartTime();

    /**
     * 是否可以回调服务，如果为true，表示可以在逻辑中调用本实例
     *
     * @return
     */
    default boolean isCallable() {
        return false;
    }
}
