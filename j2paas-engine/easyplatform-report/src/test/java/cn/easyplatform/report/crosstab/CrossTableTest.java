
package cn.easyplatform.report.crosstab;

import java.util.HashMap;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

import org.junit.Test;

import cn.easyplatform.report.AbstractReportTest;
import cn.easyplatform.report.JREasyPlatformScriptlet;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CrossTableTest extends AbstractReportTest {

	@Test
	public void test() throws Exception {
		JasperReport jr = JasperCompileManager
				.compileReport(CrossTableTest.class
						.getResourceAsStream("crosstab_1.jrxml"));
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_CONNECTION", getConn());
		parameters.put("REPORT_SCRIPTLET", new JREasyPlatformScriptlet());
		JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parameters);
		JasperViewer viewer = new JasperViewer(jasperPrint);
		viewer.setVisible(true);
		synchronized (this) {
			this.wait();
		}
	}
}
