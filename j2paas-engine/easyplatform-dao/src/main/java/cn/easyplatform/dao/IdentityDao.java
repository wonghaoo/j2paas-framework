/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.OrgDo;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.messages.vos.MenuVo;
import cn.easyplatform.messages.vos.OrgVo;
import cn.easyplatform.messages.vos.RoleVo;

import java.util.List;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IdentityDao {

    /**
     * @param userId
     * @param cb
     * @return
     */
    <T extends BaseEntity> List<RoleVo> getUserRoles(String userId,
                                                     String deviceType, EntityCallback<T> cb);

    /**
     * @param orgId
     * @param userId
     * @param deviceType
     * @param cb
     * @return
     */
    <T extends BaseEntity> List<RoleVo> getUserRoles(String orgId,
                                                     String userId, String deviceType, EntityCallback<T> cb);

    /**
     * @param orgId
     * @param userId
     * @param deviceType
     * @param cb
     * @return
     */
    <T extends BaseEntity> List<RoleVo> getUserOrgRoles(String orgId,
                                                     String userId, String deviceType, EntityCallback<T> cb);

    /**
     * @param userId
     * @return
     */
    <T extends BaseEntity> List<OrgVo> getUserOrgs(String userId,
                                                   EntityCallback<T> cb);

    /**
     * @param query 由系统配置的查询语句，参考easyplatform.conf
     * @param orgId
     * @return
     */
    <T extends BaseEntity> OrgDo getUserOrg(String query, String orgId);

    /**
     * @param query  由系统配置的查询语句，参考easyplatform.conf
     * @param userId
     * @return
     */
    UserDo getUser(String query, String userId);

    /**
     * @param query
     * @param userId
     * @return
     */
    List<String[]> getAgent(String query, List<FieldDo> params);

    /**
     * @param query
     * @param userId
     * @return
     */
    <T extends BaseEntity> List<RoleVo> getRole(String query,
                                                List<FieldDo> params, EntityCallback<T> cb);

    /**
     * @param roleId
     * @param cb
     * @return
     */
    <T extends BaseEntity> List<MenuVo> getRoleMenu(String roleId, String deviceType,
                                                    EntityCallback<T> cb);

    /**
     * @param userId
     * @return
     */
    int getLoginFailedTimes(String userId);

    /**
     * @param user
     */
    void setUser(UserDo user);

    /**
     * @param userId
     * @param times
     */
    void updateLoginFailedTimes(String userId, int times, String ip);

    /**
     * @param userId
     * @param state
     */
    void updateState(String userId, String address, int state);

    /**
     * @param userId
     * @param password
     */
    void updatePassword(String userId, String password);

    /**
     * @param statement
     * @param parameter
     * @return
     */
    Map<String, Object> selectOne(String statement, Object... parameter);

    /**
     * 获取功能的页面权限
     *
     * @param roleId
     * @param taskId
     * @param type
     * @return
     */
    String getAccess(String roleId, String taskId, String type);
}
