/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services.cache;

import net.sf.ehcache.Element;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EhCache<K, V> implements Cache<K, V> {

	private static final Logger log = LoggerFactory.getLogger(EhCache.class);

	private net.sf.ehcache.Ehcache cache;

	public EhCache(net.sf.ehcache.Ehcache cache) {
		if (cache == null) {
			throw new IllegalArgumentException("Cache argument cannot be null.");
		}
		this.cache = cache;
	}

	@SuppressWarnings("unchecked")
	public V get(K key) throws CacheException {
		try {
			if (log.isTraceEnabled()) {
				log.trace("Getting object from cache [" + cache.getName()
						+ "] for key [" + key + "]");
			}
			if (key == null) {
				return null;
			} else {
				Element element = cache.get(key);
				if (element == null) {
					if (log.isTraceEnabled()) {
						log.trace("Element for [" + key + "] is null.");
					}
					return null;
				} else {
					return (V) element.getObjectValue();
				}
			}
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public V put(K key, V value) throws CacheException {
		if (log.isTraceEnabled()) {
			log.trace("Putting object in cache [" + cache.getName()
					+ "] for key [" + key + "]");
		}
		try {
			V previous = get(key);
			Element element = new Element(key, value);
			cache.put(element);
			return previous;
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public V remove(K key) throws CacheException {
		if (log.isTraceEnabled()) {
			log.trace("Removing object from cache [" + cache.getName()
					+ "] for key [" + key + "]");
		}
		try {
			V previous = get(key);
			cache.remove(key);
			return previous;
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public void clear() throws CacheException {
		if (log.isTraceEnabled()) {
			log.trace("Clearing all objects from cache [" + cache.getName()
					+ "]");
		}
		try {
			cache.removeAll();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public int size() {
		try {
			return cache.getSize();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public Set<K> keys() {
		try {
			@SuppressWarnings({ "unchecked" })
			List<K> keys = cache.getKeys();
			if (!CollectionUtils.isEmpty(keys)) {
				return Collections.unmodifiableSet(new LinkedHashSet<K>(keys));
			} else {
				return Collections.emptySet();
			}
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public Collection<V> values() {
		try {
			@SuppressWarnings({ "unchecked" })
			List<K> keys = cache.getKeys();
			if (!CollectionUtils.isEmpty(keys)) {
				List<V> values = new ArrayList<V>(keys.size());
				for (K key : keys) {
					V value = get(key);
					if (value != null) {
						values.add(value);
					}
				}
				return Collections.unmodifiableList(values);
			} else {
				return Collections.emptyList();
			}
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	public long getMemoryUsage() {
		try {
			return cache.calculateInMemorySize();
		} catch (Throwable t) {
			return -1;
		}
	}

	public long getMemoryStoreSize() {
		try {
			return cache.getMemoryStoreSize();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	/**
	 * @return
	 */
	public long getDiskStoreSize() {
		try {
			return cache.getDiskStoreSize();
		} catch (Throwable t) {
			throw new CacheException(t);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "EhCache [" + cache.getName() + "]";
	}
}
