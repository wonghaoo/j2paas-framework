function create() {
    $.create();
}

function copy() {
    $.copy();
}

function remove() {
    $.remove();
}

function edit() {
    $.edit();
}

function read() {
    $.read();
}

function open() {
    if (arguments.length == 0) {
        $.read();
    } else {
        var code = arguments[0];
        if (code == 'C')
            $.create();
        else if (code == 'U')
            $.edit();
        else if (code == 'D')
            $.remove();
        else
            $.read();
    }
}

function commit() {
    $.commit();
}

function save() {
    $.save();
}

function next() {
    if (arguments.length == 1)
        $.next(arguments[0]);
    else
        $.next(null);
}

function prev() {
    $.prev();
}

function reload() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.reload(args);
}

function update() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.update(args);
}

function reset() {
    $.reset();
}

function refresh() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.refresh(args);
}

function go() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.go(args);
}

function close() {
    $.close();
}

function evalLogic() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    return $.evalExpression(arguments[0], 1, args);
}

function executeUpdate() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    return $.evalExpression(arguments[0], 4, args);
}

function evalExpr() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    return $.evalExpression(arguments[0], 5, args);
}

function postEvent() {
    if (arguments.length == 1)
        $.postEvent(arguments[0], null, null);
    else if (arguments.length == 2)
        $.postEvent(arguments[0], arguments[1], null);
    else if (arguments.length == 3)
        $.postEvent(arguments[0], arguments[1], arguments[2]);
}

function check() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    return $.check(arguments[0], args);
}

function print() {
    if (arguments.length == 1)
        $.print(arguments[0], null, null);
    else if (arguments.length == 2)
        $.print(arguments[0], arguments[1], null);
    else if (arguments.length == 3)
        $.print(arguments[0], arguments[1], arguments[2]);
    else
        $.print(null, null, null);
}

function showCode() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    $.showCode(arguments[0], args);
}

function showConfirm(msg) {
    $.showMessage(1, msg);
}

function showWarn(msg) {
    $.showMessage(2, msg);
}

function showInfo(msg) {
    $.showMessage(3, msg);
}

function showError(msg) {
    $.showMessage(4, msg);
}

function showWrongValue(c, msg) {
    $.showWrongValue(c, msg);
}

function showNotification() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.showNotification(args);
}

function getData() {
    if (arguments.length == 0)
        return $.getData(null);
    else if (arguments.length == 1)
        return $.getData(arguments[0]);
}

function setData() {
    if (arguments.length == 1)
        $.setData(null, arguments[0], true);
    else if (arguments.length == 2)
        $.setData(arguments[0], arguments[1], true);
    else if (arguments.length == 3)
        $.setData(arguments[0], arguments[1], arguments[2]);
}

function download() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.download(args);
}

function chat(text, user) {
    $.chat(text, user);
}

function send() {
    if (arguments.length == 2)
        return $.send(arguments[0], arguments[1]);
    else if (arguments.length > 2) {
        var args = new Array();
        for (var i = 2; i < arguments.length; i++)
            args[i - 2] = arguments[i];
        return $.send(arguments[0], arguments[1], args);
    }
}

function logout() {
    $.logout();
}

function get$(n) {
    return $.get$(n);
}

function set$(n, v) {
    $.set$(n, v);
}

function get0$(n) {
    return $.get0$(n);
}

function set0$(n, v) {
    $.set0$(n, v);
}

function host0$() {
    return $.host0$();
}

function host$() {
    return $.host$();
}

function setDisabled(c, disabled) {
    $.setDisabled(c, disabled);
}

function setReadonly(c, readonly) {
    $.setReadonly(c, readonly);
}

function batch() {
    if (arguments.length == 1)
        $.batch(arguments[0]);
    else
        $.batch(null);
}

function paging() {
    if (arguments.length == 1)
        $.paging(arguments[0]);
    else
        $.paging(1);
}

function query() {
    $.query();
}

function evalList() {
    var args = new Array();
    for (var i = 0; i < arguments.length; i++)
        args[i] = arguments[i];
    $.evalList(args);
}

function bind(name, value) {
    $.bind(name, value);
}

function sort(name, isAscending) {
    $.sort(name, isAscending);
}

function subscribe(topic) {
    $.subscribe(topic);
}

function unsubscribe() {
    $.unsubscribe();
}

function login() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    $.login(arguments[0], args);
}