/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller.admin;

import cn.easyplatform.messages.vos.LoginVo;
import cn.easyplatform.web.Version;
import cn.easyplatform.web.contexts.Contexts;
import org.zkoss.util.resource.Labels;
import org.zkoss.xel.util.SimpleResolver;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zk.ui.util.ComposerExt;
import org.zkoss.zul.A;
import org.zkoss.zul.Label;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PreivewLayoutController implements Composer, ComposerExt<Component> {

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component component,
                                         ComponentInfo componentinfo) throws Exception {
        Map<String, Object> vars = new HashMap<String, Object>();
        vars.put("$easyplatform", this);
        page.addVariableResolver(new SimpleResolver(vars));
        return componentinfo;
    }

    @Override
    public void doBeforeComposeChildren(Component comp) throws Exception {

    }

    @Override
    public boolean doCatch(Throwable throwable) throws Exception {
        return false;
    }

    @Override
    public void doFinally() throws Exception {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {

    }

    public void logout() {

    }

    public void go(String name) {

    }

    public void main(String taskId) {

    }

    public void home(String taskId) {

    }

    public void title(Component c) {
        if (c instanceof Label) {
            Label header = (Label) c;
            header.setValue(Labels.getLabel("admin.title", new String[]{Version.VERSION}));
            header.setTooltiptext(Labels.getLabel("admin.build", new String[]{Version.TIMESTAMP, Version.SCM_REVISION}));
        } else if (c instanceof A) {
            A a = (A) c;
            a.setLabel(Labels.getLabel("admin.title", new String[]{Version.VERSION}));
            a.setTooltiptext(Labels.getLabel("admin.build", new String[]{Version.TIMESTAMP, Version.SCM_REVISION}));
        }
    }

    public void close(Component ref, int type) {

    }

    /**
     * 系统设置
     *
     * @param c
     */
    public void setting(Component c) {

    }

    /**
     * 改变密码
     *
     * @param c
     */
    public void changepwd(Component c) {

    }

    /**
     * 设定用户
     *
     * @param c
     */
    public void user(Component c) {
    }

    /**
     * 当前用户
     *
     * @return
     */
    public LoginVo getUser() {
        return Contexts.getUser();
    }

    /**
     * 主页功能
     *
     * @param comp
     * @param taskId
     */
    public void task(Component comp, String taskId) {

    }

    /**
     * 主页功能
     *
     * @param comp
     * @param taskId
     * @param title  是否要显示标题
     */
    public void task(Component comp, String taskId, boolean title) {

    }

    /**
     * 主页功能
     *
     * @param comp
     * @param taskId
     * @param roles  符合指定的角色才可能显示
     */
    public void task(Component comp, String taskId, String roles) {

    }

    /**
     * 主页功能
     *
     * @param comp
     * @param taskId
     * @param title  是否要显示标题
     * @param roles  符合指定的角色才可能显示
     */
    public void task(final Component comp, final String taskId,
                     final boolean title, final String roles) {

    }


    /*
     *以下为预览页面时需要的方法
     */
    public void prev() {

    }

    public void header(Component component) {

    }

    public void home() {

    }

    public void me() {

    }

    /**
     * 消息
     *
     * @param comp
     */
    public void message(Component comp) {
    }

    public void message(Component comp, String type) {

    }
    /**
     * 待办事项
     *
     * @param comp
     */
    public void bpm(Component comp) {
    }

    public void menu(Component comp) {

    }

    public void task(String taskId) {

    }

    public boolean isMobile() {
        return false;
    }

    /**
     * 当前机构
     *
     * @return
     */
    public String getOrgId() {
        return "administrator";
    }

    /**
     * 机构名称
     *
     * @return
     */
    public String getOrgName() {
        return "administrator";
    }

    /**
     * 判断给定的角色是否在当前用户中
     *
     * @param roles
     * @return
     */
    public boolean isRole(String roles) {
        return true;
    }


    /**
     * 调试
     *
     * @param comp
     */
    public void debug(Component comp) {
    }
}
