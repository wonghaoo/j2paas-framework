/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.messages.request.GetListRequestMessage;
import cn.easyplatform.messages.response.GetListResponseMessage;
import cn.easyplatform.messages.vos.GetListVo;
import cn.easyplatform.messages.vos.datalist.ListGetListVo;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.Queryable;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.zkex.list.PanelSupport;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractQueryBuilder<T extends Queryable> implements ComponentBuilder {

    protected OperableHandler main;

    protected ListSupport support;

    protected Component anchor;

    protected T me;

    public AbstractQueryBuilder(OperableHandler mainTaskHandler, T comp) {
        this.main = mainTaskHandler;
        this.me = comp;
    }

    public AbstractQueryBuilder(ListSupport support, T comp,
                                Component anchor) {
        this.support = support;
        this.main = support.getMainHandler();
        this.anchor = anchor;
        this.me = comp;
    }

    /**
     * 加载数据
     */
    protected void load() {
        if (me.getQuery() instanceof String) {
            ComponentService cs = ServiceLocator
                    .lookup(ComponentService.class);
            GetListVo gv = null;
            if (anchor != null) {
                ListRowVo rowVo = WebUtils.getAnchorValue(anchor);
                Object[] data = support.isCustom() ? rowVo.getData() : rowVo
                        .getKeys();
                gv = new ListGetListVo(me.getDbId(), (String) me.getQuery(), support
                        .getComponent().getId(), data);
            } else if (main instanceof PanelSupport) {
                ListSupport ls = ((PanelSupport) main).getList();
                gv = new ListGetListVo(me.getDbId(), (String) me.getQuery(), ls
                        .getComponent().getId(), null);
            } else
                gv = new GetListVo(me.getDbId(), (String) me.getQuery());
            gv.setFilter(me.getFilter());
            GetListRequestMessage req = new GetListRequestMessage(main.getId(), gv);
            IResponseMessage<?> resp = cs.getList(req);
            if (resp.isSuccess()) {
                List<?> data = ((GetListResponseMessage) resp).getBody();
                createModel(data);
            } else
                Clients.wrongValue((Component) me, (String) resp.getBody());
        } else if (me.getQuery() instanceof List<?>) {
            List<Object[]> data = (List<Object[]>) me.getQuery();
            createModel(data);
        }
    }

    /**
     * 创建模型
     *
     * @param data
     */
    protected abstract void createModel(List<?> data);
}
