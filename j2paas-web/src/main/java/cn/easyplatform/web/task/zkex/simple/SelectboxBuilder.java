/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.GetListRequestMessage;
import cn.easyplatform.messages.response.GetListResponseMessage;
import cn.easyplatform.messages.vos.GetListVo;
import cn.easyplatform.messages.vos.datalist.ListGetListVo;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.PairItem;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.SelectboxExt;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.zkex.list.PanelSupport;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectboxBuilder extends AbstractQueryBuilder<SelectboxExt> implements Widget {

    public SelectboxBuilder(OperableHandler mainTaskHandler, SelectboxExt comp) {
        super(mainTaskHandler, comp);
    }

    public SelectboxBuilder(ListSupport support, SelectboxExt comp, Component anchor) {
        super(support, comp, anchor);
    }

    @Override
    public Component build() {
        me.setAttribute("$proxy", this);
        PageUtils.checkAccess(main.getAccess(), me);
        Object value = me.getValue();
        if (me.isImmediate()) {
            load();
            me.setValue(value);
        }
        PageUtils.processEventHandler(main, me, anchor);
        return me;
    }

    protected void createModel(List<?> data) {
        ListModelList<PairItem> model = new ListModelList<PairItem>();
        if (me.getEmptyLabel() != null
                && !me.getEmptyLabel().equals("")) {
            PairItem item = new PairItem(me.getEmptyValue(),
                    me.getEmptyLabel());
            model.add(item);
        }
        for (Object fv : data) {
            Object[] fvs = (Object[]) fv;
            PairItem item = null;
            if (fvs.length == 1) {
                item = new PairItem(fvs[0] instanceof FieldVo ? ((FieldVo) fvs[0]).getValue() : fvs[0]);
            } else if (fvs.length > 1) {
                boolean isRaw = fvs[0] instanceof FieldVo;
                item = new PairItem(isRaw ? ((FieldVo) fvs[0]).getValue() : fvs[0],
                        isRaw ? (String) ((FieldVo) fvs[1]).getValue() : (String) fvs[1]);
            }
            model.add(item);
        }
        me.setModel(model);
    }

    @Override
    public void reload(Component me) {
        me.getChildren().clear();
        load();
    }
}
