/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.calendar.Calendars;
import cn.easyplatform.web.ext.calendar.api.CalendarEvent;
import cn.easyplatform.web.ext.calendar.event.CalendarsEvent;
import cn.easyplatform.web.ext.calendar.impl.SimpleCalendarEvent;
import cn.easyplatform.web.ext.calendar.impl.SimpleCalendarModel;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.event.EventListenerHandler;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CalendarsBuilder extends AbstractQueryBuilder<Calendars> implements Widget {

    private List<CalendarEvent> cm;

    public CalendarsBuilder(OperableHandler mainTaskHandler, Calendars calendars) {
        super(mainTaskHandler, calendars);
    }

    @Override
    public Component build() {
//        if (Strings.isBlank(me.getQuery()))
//            throw new EasyPlatformWithLabelKeyException(
//                    "component.property.not.found", "<calendars>", "query");
        me.setAttribute("$proxy", this);
        if (!Strings.isBlank(me.getCreateEvent()))
            me.addEventListener(CalendarsEvent.ON_EVENT_CREATE,
                    new EventListenerHandler(CalendarsEvent.ON_EVENT_CREATE, support == null ? main : support, me.getCreateEvent(), anchor));
        if (!Strings.isBlank(me.getEvent())) {
            EventListener<CalendarsEvent> el = new EventListener<CalendarsEvent>() {
                @Override
                public void onEvent(CalendarsEvent event) throws Exception {
                    EventListenerHandler elh = new EventListenerHandler(CalendarsEvent.ON_EVENT_UPDATE, support == null ? main : support,
                            me.getEvent(), anchor);
                    elh.onEvent(event);
                    // 只有上面成功才更新
                    Map<String, Object> data = new HashMap<String, Object>();
                    data.put("id", event.getId());
                    data.put("beginDate", event.getBeginDate());
                    data.put("endDate", event.getEndDate());
                    me.updateEvent(data);
                }
            };
            me.addEventListener(CalendarsEvent.ON_EVENT_UPDATE, el);
        }
        if (!Strings.isBlank(me.getEditEvent()))
            me.addEventListener(CalendarsEvent.ON_EVENT_EDIT,
                    new EventListenerHandler(CalendarsEvent.ON_EVENT_EDIT, main, me.getEditEvent(), anchor));
        if (!Strings.isBlank(me.getDayEvent()))
            me.addEventListener(CalendarsEvent.ON_DAY_CLICK,
                    new EventListenerHandler(CalendarsEvent.ON_DAY_CLICK, main, me.getDayEvent(), anchor));
        if (!Strings.isBlank(me.getWeekEvent()))
            me.addEventListener(CalendarsEvent.ON_WEEK_CLICK,
                    new EventListenerHandler(CalendarsEvent.ON_WEEK_CLICK, main, me.getWeekEvent(), anchor));
        if (!Strings.isBlank(me.getDndEvent()))
            me.addEventListener(Events.ON_DROP,
                    new EventListenerHandler(Events.ON_DROP, main, me.getDndEvent(), anchor));
        if (!Strings.isBlank(me.getTooltipEvent()))
            me.addEventListener(CalendarsEvent.ON_EVENT_TOOLTIP, new EventListenerHandler(CalendarsEvent.ON_EVENT_TOOLTIP, main, me.getTooltipEvent(), anchor));
        cm = new ArrayList<CalendarEvent>();
        me.setModel(new SimpleCalendarModel(cm, false));
        return me;
    }

    @Override
    protected void createModel(List<?> data) {
        cm.clear();
        for (Object row : data) {
            FieldVo[] fvs = (FieldVo[]) row;
            SimpleCalendarEvent ce = new SimpleCalendarEvent();
            for (FieldVo fv : fvs) {
                if (fv.getName().equalsIgnoreCase("id"))
                    ce.setId(fv.getValue());
                else if (fv.getName().equalsIgnoreCase("begindate"))
                    ce.setBeginDate((Date) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("enddate"))
                    ce.setEndDate((Date) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("title"))
                    ce.setTitle((String) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("content"))
                    ce.setContent((String) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("headerColor"))
                    ce.setHeaderColor((String) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("contentColor"))
                    ce.setContentColor((String) fv.getValue());
                else if (fv.getName().equalsIgnoreCase("locked")) {
                    String val = fv.getValue().toString();
                    ce.setLocked(val.equals("1")
                            || val.equalsIgnoreCase("true")
                            || val.equals("T"));
                }
            }
            cm.add(ce);
        }
        me.setModel(new SimpleCalendarModel(cm, false));
    }

    @Override
    public void reload(Component widget) {
        load();
    }

}
