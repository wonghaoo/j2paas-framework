/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.FisheyebarExt;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.event.EventListenerHandler;
import org.apache.commons.codec.binary.Base64;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Fisheye;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/7/15 15:18
 * @Modified By:
 */
public class FisheyebarBuilder extends AbstractQueryBuilder<FisheyebarExt> implements Widget {

    public FisheyebarBuilder(OperableHandler mainTaskHandler, FisheyebarExt comp) {
        super(mainTaskHandler, comp);
    }

    @Override
    public Component build() {
        load();
        me.setAttribute("$proxy", this);
        return me;
    }

    protected void createModel(List<?> data) {
        EventListenerHandler el = null;
        if (!Strings.isBlank(me.getEvent()))
            el = new EventListenerHandler(Events.ON_CLICK, main,
                    me.getEvent(), anchor);
        for (Object o : data) {
            Object[] fvs = (Object[]) o;
            Fisheye fisheye = new Fisheye();
            if (fvs[0] instanceof FieldVo) {
                if (fvs.length > 2) {
                    fisheye.setLabel((String) ((FieldVo) fvs[0]).getValue());
                    Object val = revertValue(((FieldVo) fvs[1]).getValue());
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                    val = revertValue(((FieldVo) fvs[2]).getValue());
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setHoverImage(val.toString());
                        else
                            fisheye.setHoverImageContent((Image) val);
                    }
                } else if (fvs.length > 1) {
                    fisheye.setLabel((String) ((FieldVo) fvs[0]).getValue());
                    Object val = revertValue(((FieldVo) fvs[1]).getValue());
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                } else {
                    Object val = revertValue(((FieldVo) fvs[0]).getValue());
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                }
            } else {
                if (fvs.length > 2) {
                    fisheye.setLabel((String) fvs[0]);
                    Object val = revertValue(fvs[1]);
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                    val = revertValue(fvs[2]);
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setHoverImage(val.toString());
                        else
                            fisheye.setHoverImageContent((Image) val);
                    }
                } else if (fvs.length > 1) {
                    fisheye.setLabel((String) fvs[0]);
                    Object val = revertValue(fvs[1]);
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                } else {
                    Object val = revertValue(fvs[0]);
                    if (val != null) {
                        if (val instanceof String)
                            fisheye.setImage(val.toString());
                        else
                            fisheye.setImageContent((Image) val);
                    }
                }
            }
            if (!Strings.isBlank(me.getEvent()))
                fisheye.addEventListener(Events.ON_CLICK, el);
        }
    }

    private Object revertValue(Object value) {
        if (value == null)
            return null;
        if (value instanceof byte[]) {
            try {
                return new AImage("",
                        (byte[]) value);
            } catch (IOException ex) {
            }
        }
        String src = (String) value;
        if (src.startsWith("$7")) {
            try {
                return "/servlets/download?id="
                        + URLEncoder.encode(src, "utf8");
            } catch (UnsupportedEncodingException e) {
            }
        } else if (!src.trim().equals("")) {
            if (src.length() > 200) {
                try {
                    return new AImage("",
                            (Base64.decodeBase64(src)));
                } catch (IOException ex) {
                }
            } else {
                return src;
            }
        }
        return null;
    }

    @Override
    public void reload(Component widget) {
        me.getChildren().clear();
        load();
    }
}
