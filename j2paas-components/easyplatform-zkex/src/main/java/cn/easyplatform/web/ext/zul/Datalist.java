/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.Cacheable;
import cn.easyplatform.web.ext.EntityExt;
import cn.easyplatform.web.ext.ManagedExt;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Datalist extends Div implements EntityExt, ManagedExt, Cacheable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 是否显示标题
     */
    private boolean _showTitle = true;

    /**
     * 类型：
     */
    private String _type = "catalog";

    /**
     * 初始脚本，仅对DETAIL和CATALOG类型有效
     */
    private String _init;

    /**
     * 记录添加到列表之前执行
     */
    private String _before;

    /**
     * 记录添加到列表之后执行
     */
    private String _after;

    /**
     * 右键菜单－>复制和粘贴，值为空表示不显示右键菜单，否则显示复制/粘贴菜单，此时的值表示菜单名称
     * 例如：值为"费用明细表"，那么真实的菜单为"复制费用明细表"和"粘贴费用明细表"
     */
    private String _contextMenu;

    /**
     * 可编辑列的名称,Header.name,多个用逗号分隔
     */
    private String _editableColumns;

    /**
     * 不可视的列名称，Header.name,多个用逗号分隔
     */
    private String _invisibleColumns;

    /**
     * 在显示时是否要马上执行查询
     */
    private boolean _immediate = true;

    /**
     * 编辑栏位是否设为inplace模式
     */
    private boolean _inplace;

    /**
     * 打开对应页面的id
     */
    private String _pageId;

    /**
     * 打开方式，参考Constants.OPEN_XXXX，默认为模式页面
     */
    private int _openModel = 3;

    /**
     * 目标容器，仅当openModel == OPEN_EMBBED 时才有效
     */
    private String _container;

    /**
     * 列表数据来源自页面上的另外一个列表记录
     */
    private String _host;

    /**
     * 当页面重载时是否重新加载数据，这时候列表变成单纯的页面记录选择器,type=CATALOG时使用
     */
    private boolean _durable;

    /**
     * 是否显示选择所有
     */
    private boolean _checkall;

    /**
     * 是否使用自定义查询的数据作为记录
     */
    private boolean _useQueryResult;

    /**
     * 可否要锁住记录
     */
    private boolean _lock = false;

    /**
     * 是否可以打印
     */
    private boolean _print = false;

    /**
     * 对应参数DATALIST的id
     */
    private String _entity;

    /**
     * 是否显示查询面板
     */
    private boolean _showPanel;

    /**
     * 显示行数
     */
    private boolean _showRowNumbers;

    /**
     * 每一页的笔数
     */
    private int _pageSize;

    /**
     * 分组名称：参考参数Group.name
     */
    private String _group;

    /**
     * 排序，如果值不为空，替换list参数中的排序栏位
     */
    private String _orderBy;

    /**
     * 栏位之间形成的层级结构，由2个栏位组成,第1个层级字段，第2个是父层字段
     */
    private String _levelBy;

    /**
     * 列头可否调整大小
     */
    private boolean _sizable = true;

    /**
     * 冻结的列数，从左到右开始
     */
    private int _frozenColumns;

    /**
     * 从哪一列开始
     */
    private int _frozenStart;

    /**
     * 右边冻结的列数
     */
    private int _frozenRightColumns;

    /**
     * 是否可以导出文件
     */
    private boolean _export = true;

    /**
     * 分页组件是否显示详细信息
     */
    private boolean _pagingDetailed = true;

    /**
     * 分页组件是否自动显示
     */
    private boolean _pagingAutohide = true;

    /**
     * 分页组件显示模式
     */
    private String _pagingMold;

    /**
     * 分页组件显示style
     */
    private String _pagingStyle;

    /**
     * 是否显示分页栏
     */
    private boolean _showPaging = true;

    /**
     * 由单元格的内容决定单元格宽度
     */
    private boolean _sizedByContent;

    /**
     * 样式
     */
    private String _mold = "default";

    /**
     * 是否填充右边空余空间
     */
    private boolean _span;

    /**
     * 是否可选择
     */
    private boolean _checkmark;

    /**
     * 是否可多选
     */
    private boolean _multiple;

    /**
     * 列表间隔行风格
     */
    private String _oddRowSclass;

    /**
     * 显示几行
     */
    private int _rows;

    /**
     * 是否显示分组笔数
     */
    private boolean _showGroupCount;

    /**
     * 条件
     */
    private String _condition;

    /**
     * 行风格
     */
    private String _rowStyle;

    /**
     * 头部风格
     */
    private String _headStyle;

    /**
     *
     */
    private String _auxHeadStyle;

    /**
     * 底部风格
     */
    private String _footStyle;

    /**
     * 冻结头部风格
     */
    private String _frozenStyle = "background: #dfded8";

    /**
     * 排序栏拉,根据这个栏位来执行first、next、previous、last方法
     */
    private String _sortField;

    /**
     * 过滤表达式
     */
    private String _filter;

    /**
     * 当列表里面有按钮等其它非编辑组件，需要触发列表的选择事件，就需要设置这个值，参考listbox.nonselectableTags
     */
    private String _nonselectableTags;

    /**
     * 初始打开的层次
     */
    private int _depth;

    /**
     * 是否使用session缓存
     */
    private boolean _cache;
    /**
     * 当pageSize大于0，fetchAll为true，表示获取所有的数据，不需要分页，使用本身的分页
     */
    private boolean _fetchAll;

    /**
     * 延时加载where
     */
    private String _lazyQuery;

    private String _emptyMessage;

    public String getEmptyMessage() {
        return _emptyMessage;
    }

    public void setEmptyMessage(String emptyMessage) {
        this._emptyMessage = emptyMessage;
    }

    /**
     * @return the fetchAll
     */
    public boolean isFetchAll() {
        return _fetchAll;
    }

    /**
     * @param fetchAll the fetchAll to set
     */
    public void setFetchAll(boolean fetchAll) {
        this._fetchAll = fetchAll;
    }

    public boolean isCache() {
        return _cache;
    }

    public void setCache(boolean cache) {
        this._cache = cache;
    }

    /**
     * @return the depth
     */
    public int getDepth() {
        return _depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(int depth) {
        this._depth = depth;
    }

    /**
     * @return the nonselectableTags
     */
    public String getNonselectableTags() {
        return _nonselectableTags;
    }

    /**
     * @param nonselectableTags the nonselectableTags to set
     */
    public void setNonselectableTags(String nonselectableTags) {
        this._nonselectableTags = nonselectableTags;
    }

    /**
     * @return the useQueryResult
     */
    public boolean isUseQueryResult() {
        return _useQueryResult;
    }

    /**
     * @param useQueryResult the useQueryResult to set
     */
    public void setUseQueryResult(boolean useQueryResult) {
        this._useQueryResult = useQueryResult;
    }

    /**
     * @return the checkall
     */
    public boolean isCheckall() {
        return _checkall;
    }

    /**
     * @param checkall the checkall to set
     */
    public void setCheckall(boolean checkall) {
        this._checkall = checkall;
    }

    /**
     * @return the print
     */
    public boolean isPrint() {
        return _print;
    }

    /**
     * @param print the print to set
     */
    public void setPrint(boolean print) {
        this._print = print;
    }

    /**
     * @return the condition
     */
    public String getCondition() {
        return _condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(String condition) {
        this._condition = condition;
    }

    /**
     * @return the showGroupCount
     */
    public boolean isShowGroupCount() {
        return _showGroupCount;
    }

    /**
     * @param showGroupCount the showGroupCount to set
     */
    public void setShowGroupCount(boolean showGroupCount) {
        this._showGroupCount = showGroupCount;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return _orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this._orderBy = orderBy;
    }

    /**
     * @return the _levelBy
     */
    public String getLevelBy() {
        return _levelBy;
    }

    /**
     * @param levelBy the _levelBy to set
     */
    public void setLevelBy(String levelBy) {
        this._levelBy = levelBy;
    }

    /**
     * @return the rows
     */
    public int getRows() {
        return _rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(int rows) {
        this._rows = rows;
    }

    /**
     * @return the oddRowSclass
     */
    public String getOddRowSclass() {
        return _oddRowSclass;
    }

    /**
     * @param oddRowSclass the oddRowSclass to set
     */
    public void setOddRowSclass(String oddRowSclass) {
        this._oddRowSclass = oddRowSclass;
    }

    /**
     * @return the checkmark
     */
    public boolean isCheckmark() {
        return _checkmark;
    }

    /**
     * @param checkmark the checkmark to set
     */
    public void setCheckmark(boolean checkmark) {
        this._checkmark = checkmark;
    }

    /**
     * @return the multiple
     */
    public boolean isMultiple() {
        return _multiple;
    }

    /**
     * @param multiple the multiple to set
     */
    public void setMultiple(boolean multiple) {
        this._multiple = multiple;
    }

    /**
     * @return the pagingMold
     */
    public String getPagingMold() {
        return _pagingMold;
    }

    /**
     * @param pagingMold the pagingMold to set
     */
    public void setPagingMold(String pagingMold) {
        this._pagingMold = pagingMold;
    }

    /**
     * @return the pagingStyle
     */
    public String getPagingStyle() {
        return _pagingStyle;
    }

    /**
     * @param pagingStyle the pagingStyle to set
     */
    public void setPagingStyle(String pagingStyle) {
        this._pagingStyle = pagingStyle;
    }

    /**
     * @return the pagingAutohide
     */
    public boolean isPagingAutohide() {
        return _pagingAutohide;
    }

    /**
     * @param pagingAutohide the pagingAutohide to set
     */
    public void setPagingAutohide(boolean pagingAutohide) {
        this._pagingAutohide = pagingAutohide;
    }

    /**
     * @return the pagingDetailed
     */
    public boolean isPagingDetailed() {
        return _pagingDetailed;
    }

    /**
     * @param pagingDetailed the pagingDetailed to set
     */
    public void setPagingDetailed(boolean pagingDetailed) {
        this._pagingDetailed = pagingDetailed;
    }

    public boolean isExport() {
        return _export;
    }

    public void setExport(boolean export) {
        this._export = export;
    }

    public int getFrozenColumns() {
        return _frozenColumns;
    }

    public void setFrozenColumns(int frozenColumns) {
        this._frozenColumns = frozenColumns;
    }

    public int getFrozenStart() {
        return _frozenStart;
    }

    public void setFrozenStart(int frozenStart) {
        this._frozenStart = frozenStart;
    }

    public int getFrozenRightColumns() {
        return _frozenRightColumns;
    }

    public void setFrozenRightColumns(int frozenRightColumns) {
        this._frozenRightColumns = frozenRightColumns;
    }

    public boolean isSizable() {
        return _sizable;
    }

    public void setSizable(boolean sizable) {
        this._sizable = sizable;
    }

    public String getPageId() {
        return _pageId;
    }

    public void setPageId(String pageId) {
        this._pageId = pageId;
    }

    public int getOpenModel() {
        return _openModel;
    }

    public void setOpenModel(int openModel) {
        this._openModel = openModel;
    }

    public String getContainer() {
        return _container;
    }

    public void setContainer(String container) {
        this._container = container;
    }

    public String getEntity() {
        return _entity;
    }

    public void setEntity(String entity) {
        this._entity = entity;
    }

    /**
     * @return the init
     */
    public String getInit() {
        return _init;
    }

    /**
     * @param init the init to set
     */
    public void setInit(String init) {
        this._init = init;
    }

    public String getBefore() {
        return _before;
    }

    public void setBefore(String before) {
        this._before = before;
    }

    public String getAfter() {
        return _after;
    }

    public void setAfter(String after) {
        this._after = after;
    }

    public String getContextMenu() {
        return _contextMenu;
    }

    public void setContextMenu(String contextMenu) {
        this._contextMenu = contextMenu;
    }

    public boolean isShowPanel() {
        return _showPanel;
    }

    public void setShowPanel(boolean showPanel) {
        this._showPanel = showPanel;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }

    public boolean isShowRowNumbers() {
        return _showRowNumbers;
    }

    public void setShowRowNumbers(boolean showRowNumbers) {
        this._showRowNumbers = showRowNumbers;
    }

    public String getEditableColumns() {
        return _editableColumns;
    }

    public void setEditableColumns(String editableColumns) {
        this._editableColumns = editableColumns;
    }

    public String getInvisibleColumns() {
        return _invisibleColumns;
    }

    public void setInvisibleColumns(String invisibleColumns) {
        this._invisibleColumns = invisibleColumns;
    }

    public int getPageSize() {
        return _pageSize;
    }

    public void setPageSize(int pgsz) {
        this._pageSize = pgsz;
    }

    public boolean isInplace() {
        return _inplace;
    }

    public void setInplace(boolean inplace) {
        this._inplace = inplace;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    public String getHost() {
        return _host;
    }

    public void setHost(String host) {
        this._host = host;
    }

    public String getGroup() {
        return _group;
    }

    public void setGroup(String group) {
        this._group = group;
    }

    public boolean isDurable() {
        return _durable;
    }

    public void setDurable(boolean durable) {
        this._durable = durable;
    }

    /**
     * @return the sizedByContent
     */
    public boolean isSizedByContent() {
        return _sizedByContent;
    }

    /**
     * @param sizedByContent the sizedByContent to set
     */
    public void setSizedByContent(boolean sizedByContent) {
        this._sizedByContent = sizedByContent;
    }

    /**
     * @return the span
     */
    public boolean isSpan() {
        return _span;
    }

    /**
     * @param span the span to set
     */
    public void setSpan(boolean span) {
        this._span = span;
    }

    /**
     * @return the mold
     */
    public String getMold() {
        return _mold;
    }

    /**
     * @param mold the mold to set
     */
    public void setMold(String mold) {
        this._mold = mold;
    }

    /**
     * @return the showTitle
     */
    public boolean isShowTitle() {
        return _showTitle;
    }

    /**
     * @param showTitle the showTitle to set
     */
    public void setShowTitle(boolean showTitle) {
        this._showTitle = showTitle;
    }

    public String getRowStyle() {
        return _rowStyle;
    }

    public void setRowStyle(String rowStyle) {
        this._rowStyle = rowStyle;
    }

    public String getHeadStyle() {
        return _headStyle;
    }

    public void setHeadStyle(String headStyle) {
        this._headStyle = headStyle;
    }

    public String getAuxHeadStyle() {
        return _auxHeadStyle;
    }

    public void setAuxHeadStyle(String auxHeadStyle) {
        this._auxHeadStyle = auxHeadStyle;
    }

    public String getFootStyle() {
        return _footStyle;
    }

    public void setFootStyle(String footStyle) {
        this._footStyle = footStyle;
    }

    public String getFrozenStyle() {
        return _frozenStyle;
    }

    public void setFrozenStyle(String frozenStyle) {
        this._frozenStyle = frozenStyle;
    }

    /**
     * @return
     */
    public boolean isLock() {
        return _lock;
    }

    /**
     * @param lock
     */
    public void setLock(boolean lock) {
        this._lock = lock;
    }

    /**
     * @return
     */
    public boolean isShowPaging() {
        return _showPaging;
    }

    /**
     * @param showPaging
     */
    public void setShowPaging(boolean showPaging) {
        this._showPaging = showPaging;
    }

    public void beforeChildAdded(Component child, Component refChild) {
        if (!(child instanceof Grid))//在设计器里要模拟显示,需要Grid
            throw new UiException((new StringBuilder())
                    .append("Unsupported child: ").append(child).toString());
    }

    /**
     * @return
     */
    public String getSortField() {
        return _sortField;
    }

    /**
     * @param sortField
     */
    public void setSortField(String sortField) {
        this._sortField = sortField;
    }

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    /**
     * @return the lazy
     */
    public String getLazyQuery() {
        return _lazyQuery;
    }

    /**
     * @param lazyQuery the lazy to set
     */
    public void setLazyQuery(String lazyQuery) {
        this._lazyQuery = lazyQuery;
    }
}
