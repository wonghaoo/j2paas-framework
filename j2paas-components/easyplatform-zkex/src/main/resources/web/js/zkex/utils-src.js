$(document).keydown(function (e) {
    var keyEvent;
    if (e.keyCode == 8) {
        var d = e.srcElement || e.target;
        if (d.tagName.toUpperCase() == 'INPUT' || d.tagName.toUpperCase() == 'TEXTAREA') {
            keyEvent = d.readOnly || d.disabled;
        } else {
            keyEvent = true;
        }
    } else {
        keyEvent = false;
    }
    if (keyEvent) {
        e.preventDefault();
    }
});
zk.meterUpdate = function (e, desc) {
    var score = strengthMeasure(e.value),
        meter = $("$meter"),
        meterWidget = zk.Widget.$(meter);
    switch (score) {
        case 1:
        case 2:
            meterWidget.setSclass("meter meter-red");
            break;
        case 3:
        case 4:
            meterWidget.setSclass("meter meter-orange");
            break;
        case 5:
        case 6:
            meterWidget.setSclass("meter meter-green");
            break;
        default:
            meterWidget.setSclass("meter");
    }
    zk.Widget.$($(".meter-inner")).setWidth(score * meter.width() / desc.length + "px");
    zk.Widget.$("$msg").setValue(desc[score]);
}

function strengthMeasure(text) {
    var score = 0;
    if (text.length > 0)
        score++;
    if (text.length > 6)
        score++;
    if ((text.match(/[a-z]/)) && (text.match(/[A-Z]/)))
        score++;
    if (text.match(/\d+/))
        score++;
    if (text.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))
        score++;
    if (text.length > 12)
        score++;
    if (text.length == 0)
        score = 0;
    return score;
}

zk.autolining = function (event, cols, rows) {
    var data = event.value;
    if (data.length <= cols)
        return;
    data = data.replace(/\r/ig, '');
    var len = cols * rows;
    if (data.length > len)
        data = data.substr(0, len);
    var str = '';
    var pos = 0;
    var rowIndex = 0;
    for (var i = 0; i < data.length; i++) {
        var c = data.charAt(i);
        if (c == '\n') {
            rowIndex++;
            pos = 0;
            if (rowIndex == rows)
                break;
            str += c;
        } else if (pos == cols - 2) {
            rowIndex++;
            if (rowIndex == rows)
                break;
            str += '\r';
            str += c;
            pos = 1;
        } else {
            str += c;
            pos++;
        }
    }
    var wgt = event.target;
    if (wgt._node) {
        wgt._node.innerHTML = str;
        wgt.setValue(str);
        wgt.fireOnChange();
    }
    event.stop();
}
zk.print = function (uuid, uri, cssuri) {
    if (uuid && uri) {
        var wgt = zk.Widget.$(uuid),
            body = document.body,
            ifr = jq('#zk_printframe');
        if (!ifr[0]) {
            jq(body).append('<iframe id="zk_printframe" name="zk_printframe"' +
                ' style="width:0;height:0;border:0;position:fixed;"' +
                '></iframe>');
            ifr = jq('#zk_printframe');
        }
        ifr.unbind('load.ajaxsubmit').bind('load.ajaxsubmit', function () {
            setTimeout(function () {
                var iw = ifr[0].contentWindow || ifr[0];
                iw.document.body.focus();
                iw.print();
            }, 50);
        });

        jq(body).append('<form id="zk_printform" action="' + uri + '" method="post" target="zk_printframe"></form>');
        var form = jq('#zk_printform'),
            content = '<div style="width: ' + wgt.$n().offsetWidth + 'px">' + toAbsolutePath(jq(wgt.$n())[0].outerHTML) + '</div>';
        form.append(jq('<input/>').attr({name: 'printContent', value: content}));
        if (cssuri) {
            form.append(jq('<input/>').attr({name: 'printStyle', value: cssuri}));
        }
        form.submit().remove();
    } else {
        window.print();
    }
}

function toAbsolutePath(content) {
    var srcArray = content.match(/src="[\w/.]+"/gi),
        hrefArray = content.match(/href="[\w/.]+"/gi),
        link = document.createElement('a');
    if (srcArray) {
        srcArray.map(function (val) {
            if (val.substring(0, 1) != '/') {
                link.href = val.substring(5, val.lastIndexOf('"'));
                var absolute = val.replace(/src="[\w/.]+"/, 'src="' + link.protocol + "//" + link.host + link.pathname + link.search + link.hash + '"');
                content = content.replace(val, absolute);
            }
        });
    }
    if (hrefArray) {
        hrefArray.map(function (val) {
            if (val.substring(0, 1) != '/') {
                link.href = val.substring(6, val.lastIndexOf('"'));
                var absolute = val.replace(/href="[\w/.]+"/, 'href="' + link.protocol + "//" + link.host + link.pathname + link.search + link.hash + '"');
                content = content.replace(val, absolute);
            }
        });
    }
    delete link;
    return content;
}

zk.printReport = function (uuid, prompt, cfg) {
    var wgt = zk.Widget.$(uuid);
    var iw = wgt.$n().window || wgt.$n().contentWindow;
    if (zk.ie) {
        if (iw.jatoolsPrinter == null) {
            var activex = '<object id="jatoolsPrinter" classid="CLSID:B43D3361-D075-4BE2-87FE-057188254255" ' +
                'codebase="' + zk.ajaxURI('/web/js/bootstrap/jatoolsPrinter.cab#version=8,6,1,0', {au: true}) +
                '" width="0" height="0"></object>'
            jq(iw.document.body).append(activex);
        }
        try {
            iw.jatoolsPrinter.getDefaultPrinter();
            myDoc = {
                settings: eval("(" + cfg + ")"),
                documents: iw.document,
                enableScreenOnlyClass: true,
                copyrights: "杰创软件拥有版权  www.jatools.com"
            };
            iw.jatoolsPrinter.print(myDoc, prompt);
        } catch (e) {
            alert('请联系管理员安装打印控件!!');
        }
    } else {
        var body = iw.document.body;
        body.focus();
        iw.print();
    }
}
zk.launchFullScreen = function () {
    var el = document.documentElement;
    var rfs = el.requestFullScreen || el.webkitRequestFullScreen ||
        el.mozRequestFullScreen || el.msRequestFullScreen;
    if (typeof rfs != "undefined" && rfs) {
        rfs.call(el);
    } else if (typeof window.ActiveXObject != "undefined") {
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript != null) {
            wscript.SendKeys("{F11}");
        }
    }
}
zk.exitFullscreen = function () {
    var el = document;
    var cfs = el.cancelFullScreen || el.webkitCancelFullScreen ||
        el.mozCancelFullScreen || el.exitFullScreen;
    if (typeof cfs != "undefined" && cfs) {
        cfs.call(el);
    } else if (typeof window.ActiveXObject != "undefined") {
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript != null) {
            wscript.SendKeys("{F11}");
        }
    }
}

function zero_fill_hex(num, digits) {
    var s = num.toString(16);
    while (s.length < digits)
        s = "0" + s;
    return s;
}

zk.rgb2hex = function (rgb) {
    if (rgb.charAt(0) == '#')
        return rgb;
    var ds = rgb.split(/\D+/);
    var decimal = Number(ds[1]) * 65536 + Number(ds[2]) * 256 + Number(ds[3]);
    return "#" + zero_fill_hex(decimal, 6);
}