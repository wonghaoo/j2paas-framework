/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.support;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MarkLine extends Mark {
    /**
     * 标注类型
     */
    private Object symbol;
    /**
     * 标注大小
     */
    private Object symbolSize;
    /**
     * 标注的样式
     */
    private ItemStyle lineStyle;
    /**
     * 标线数值的精度，在显示平均值线的时候有用
     */
    private Integer precision;
    /**
     * 一级层叠控制
     */
    private Integer zlevel;
    /**
     * 二级层叠控制
     */
    private Integer z;

    public MarkLine zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public Integer z() {
        return this.z;
    }

    public MarkLine z(Integer z) {
        this.z = z;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public MarkLine symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public MarkLine symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public ItemStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new ItemStyle();
        return lineStyle;
    }

    public MarkLine lineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Integer precision() {
        return precision;
    }

    public MarkLine precision(Integer precision) {
        this.precision = precision;
        return this;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public ItemStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }
}
