/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic.style;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ImageStyle extends GraphicStyle {
    /**
     * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的横坐标值
     */
    private Integer x;
    /**
     * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的纵坐标值
     */
    private Integer y;
    /**
     * 图形元素的宽度
     */
    private Integer width;
    /**
     * 图形元素的高度
     */
    private Integer height;

    /**
     * 图片的内容，可以是图片的 URL，也可以是 dataURI.
     */
    private String image;

    /**
     * 获取color值
     */
    public String image() {
        return this.image;
    }

    /**
     * 设置color值
     *
     * @param image
     */
    public ImageStyle image(String image) {
        this.image = image;
        return this;
    }
    public Integer x() {
        return this.x;
    }

    public GraphicStyle x(Integer x) {
        this.x = x;
        return this;
    }

    public Integer y() {
        return this.y;
    }

    public GraphicStyle y(Integer y) {
        this.y = y;
        return this;
    }

    public Integer width() {
        return this.width;
    }

    public GraphicStyle width(Integer width) {
        this.width = width;
        return this;
    }

    public Integer height() {
        return this.height;
    }

    public GraphicStyle height(Integer height) {
        this.height = height;
        return this;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
