/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Candlestick extends Boxplot {
    private Object barWidth;
    private Object barMinWidth;
    private Object barMaxWidth;
    private Boolean large;
    private Integer largeThreshold;
    /**
     * 是否裁剪超出坐标系部分的图形，具体裁剪效果根据系列决定
     */
    private Boolean clip;
    /**
     * 渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置
     */
    private Object progressive;
    /**
     * 启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
     */
    private Object progressiveThreshold;
    /**
     * 分片的方式。可选值：
     * <p>
     * 'sequential': 按照数据的顺序分片。缺点是渲染过程不自然。
     * 'mod': 取模分片，即每个片段中的点会遍布于整个数据，从而能够视觉上均匀得渲染。
     */
    private String progressiveChunkMode;

    public Candlestick() {
        this.type("candlestick");
    }


    public String progressiveChunkMode() {
        return progressiveChunkMode;
    }

    public Candlestick progressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
        return this;
    }

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Candlestick progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return progressive;
    }

    public Candlestick progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }


    public Boolean clip() {
        return this.clip;
    }

    public Candlestick clip(Boolean clip) {
        this.clip = clip;
        return this;
    }

    public Boolean large() {
        return large;
    }

    public Candlestick large(Boolean large) {
        this.large = large;
        return this;
    }

    public Integer largeThreshold() {
        return largeThreshold;
    }

    public Candlestick largeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
        return this;
    }

    public Object barWidth() {
        return barWidth;
    }

    public Candlestick barWidth(Object barWidth) {
        this.barWidth = barWidth;
        return this;
    }

    public Object barMinWidth() {
        return barMinWidth;
    }

    public Candlestick barMinWidth(Object barMinWidth) {
        this.barMinWidth = barMinWidth;
        return this;
    }

    public Object barMaxWidth() {
        return barMaxWidth;
    }

    public Candlestick barMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
        return this;
    }

    public Object getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(Object barWidth) {
        this.barWidth = barWidth;
    }

    public Object getBarMinWidth() {
        return barMinWidth;
    }

    public void setBarMinWidth(Object barMinWidth) {
        this.barMinWidth = barMinWidth;
    }

    public Object getBarMaxWidth() {
        return barMaxWidth;
    }

    public void setBarMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
    }

    public Boolean getLarge() {
        return large;
    }

    public void setLarge(Boolean large) {
        this.large = large;
    }

    public Integer getLargeThreshold() {
        return largeThreshold;
    }

    public void setLargeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
    }

    public Boolean getClip() {
        return clip;
    }

    public void setClip(Boolean clip) {
        this.clip = clip;
    }

    public Object getProgressive() {
        return progressive;
    }

    public void setProgressive(Object progressive) {
        this.progressive = progressive;
    }

    public Object getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }

    public String getProgressiveChunkMode() {
        return progressiveChunkMode;
    }

    public void setProgressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
    }
}
