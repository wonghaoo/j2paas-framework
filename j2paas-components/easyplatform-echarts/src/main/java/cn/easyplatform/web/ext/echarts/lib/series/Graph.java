/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.data.Category;
import cn.easyplatform.web.ext.echarts.lib.data.Link;
import cn.easyplatform.web.ext.echarts.lib.series.base.Circular;
import cn.easyplatform.web.ext.echarts.lib.series.base.Force;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Graph extends Series {
    private Boolean legendHoverLink;
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer polarIndex;
    private Integer geoIndex;
    private Boolean hoverAnimation;
    private Object layout;
    private Circular circular;
    private Force force;
    private Object roam;
    private Object nodeScaleRatio;
    private Boolean draggable;
    private Boolean focusNodeAdjacency;
    private Object symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private Object edgeSymbol;
    private Object edgeSymbolSize;
    private LineStyle lineStyle;
    private LabelStyle edgeLabel;
    private Category[] categories;
    private Object[] nodes;
    private Link[] links;
    private Object[] edges;
    private Object width;
    private Object height;
    private Integer calendarIndex;
    /**
     * 如果 symbol 是 path:// 的形式，是否在缩放时保持该图形的长宽比
     */
    private Boolean symbolKeepAspect;

    public Graph() {
        type="graph";
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public Graph symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public Integer calendarIndex() {
        return calendarIndex;
    }

    public Graph calendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
        return this;
    }

    public Object height() {
        return height;
    }

    public Graph height(Object height) {
        this.height = height;
        return this;
    }

    public Object width() {
        return width;
    }

    public Graph width(Object width) {
        this.width = width;
        return this;
    }

    public Object edges() {
        return edges;
    }

    public Graph edges(Object... edges) {
        this.edges = edges;
        return this;
    }

    public Object links() {
        return links;
    }

    public Graph links(Object value) {
        if (value instanceof Link[])
            this.links = (Link[]) value;
        return this;
    }

    public Object nodes() {
        return nodes;
    }

    public Graph nodes(Object... nodes) {
        this.nodes = nodes;
        return this;
    }

    public Object categories() {
        return categories;
    }

    public Graph categories(Object value) {
        if (value instanceof Category[]) {
            this.categories = (Category[]) value;
        } else if (value instanceof String) {
            String[] vals = ((String) value).split(";");
            this.categories = new Category[vals.length];
            for (int i = 0; i < categories.length; i++)
                this.categories[i] = new Category(vals[i]);
        }
//        if (categories[0] instanceof Category) {
//            this.categories = new Category[categories.length];
//            for (int i = 0; i < categories.length; i++)
//                this.categories[i] = (Category) categories[i];
//        } else {
//            this.categories = new Category[categories.length];
//            for (int i = 0; i < categories.length; i++)
//                this.categories[i] = new Category(categories[i].toString());
//        }
        return this;
    }

    public Object edgeLabel() {
        if (edgeLabel == null)
            edgeLabel = new LabelStyle();
        return edgeLabel;
    }

    public Graph edgeLabel(LabelStyle edgeLabel) {
        this.edgeLabel = edgeLabel;
        return this;
    }

    public Object lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return lineStyle;
    }

    public Graph lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Object edgeSymbolSize() {
        return edgeSymbolSize;
    }

    public Graph edgeSymbolSize(Object edgeSymbolSize) {
        this.edgeSymbolSize = edgeSymbolSize;
        return this;
    }

    public Object edgeSymbol() {
        return edgeSymbol;
    }

    public Graph edgeSymbol(Object edgeSymbol) {
        if (edgeSymbol instanceof String)
            this.edgeSymbol = ((String) edgeSymbol).split(";");
        else
            this.edgeSymbol = edgeSymbol;
        return this;
    }

    public Object focusNodeAdjacency() {
        return focusNodeAdjacency;
    }

    public Graph focusNodeAdjacency(Boolean focusNodeAdjacency) {
        this.focusNodeAdjacency = focusNodeAdjacency;
        return this;
    }

    public Object draggable() {
        return draggable;
    }

    public Graph draggable(Boolean draggable) {
        this.draggable = draggable;
        return this;
    }

    public Object nodeScaleRatio() {
        return nodeScaleRatio;
    }

    public Graph nodeScaleRatio(Object nodeScaleRatio) {
        this.nodeScaleRatio = nodeScaleRatio;
        return this;
    }

    public Object roam() {
        return roam;
    }

    public Graph roam(Object roam) {
        this.roam = roam;
        return this;
    }

    public Force force() {
        if (force == null)
            force = new Force();
        return force;
    }

    public Graph force(Force force) {
        this.force = force;
        return this;
    }

    public Circular circular() {
        if (circular == null)
            circular = new Circular();
        return circular;
    }

    public Graph circular(Circular circular) {
        this.circular = circular;
        return this;
    }

    public Object layout() {
        return layout;
    }

    public Graph layout(Object layout) {
        this.layout = layout;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Graph coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Graph xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Graph yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Integer polarIndex() {
        return polarIndex;
    }

    public Graph polarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
        return this;
    }

    public Integer geoIndex() {
        return geoIndex;
    }

    public Graph geoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public Graph symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public Graph symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return symbolRotate;
    }

    public Graph symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return symbolOffset;
    }

    public Graph symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public Boolean hoverAnimation() {
        return hoverAnimation;
    }

    public Graph hoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Graph legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getPolarIndex() {
        return polarIndex;
    }

    public void setPolarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Boolean getHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }

    public Object getLayout() {
        return layout;
    }

    public void setLayout(Object layout) {
        this.layout = layout;
    }

    public Circular getCircular() {
        return circular;
    }

    public void setCircular(Circular circular) {
        this.circular = circular;
    }

    public Force getForce() {
        return force;
    }

    public void setForce(Force force) {
        this.force = force;
    }

    public Object getRoam() {
        return roam;
    }

    public void setRoam(Object roam) {
        this.roam = roam;
    }

    public Object getNodeScaleRatio() {
        return nodeScaleRatio;
    }

    public void setNodeScaleRatio(Object nodeScaleRatio) {
        this.nodeScaleRatio = nodeScaleRatio;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }

    public Boolean getFocusNodeAdjacency() {
        return focusNodeAdjacency;
    }

    public void setFocusNodeAdjacency(Boolean focusNodeAdjacency) {
        this.focusNodeAdjacency = focusNodeAdjacency;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Object getEdgeSymbol() {
        return edgeSymbol;
    }

    public void setEdgeSymbol(Object edgeSymbol) {
        this.edgeSymbol = edgeSymbol;
    }

    public Object getEdgeSymbolSize() {
        return edgeSymbolSize;
    }

    public void setEdgeSymbolSize(Object edgeSymbolSize) {
        this.edgeSymbolSize = edgeSymbolSize;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    public Object[] getNodes() {
        return nodes;
    }

    public void setNodes(Object[] nodes) {
        this.nodes = nodes;
    }

    public Link[] getLinks() {
        return links;
    }

    public void setLinks(Link[] links) {
        this.links = links;
    }

    public Object[] getEdges() {
        return edges;
    }

    public void setEdges(Object[] edges) {
        this.edges = edges;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public void setEdgeLabel(LabelStyle edgeLabel) {
        this.edgeLabel = edgeLabel;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }
}
