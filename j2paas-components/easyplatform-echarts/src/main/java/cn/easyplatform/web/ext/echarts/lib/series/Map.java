/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.*;
import org.zkoss.lang.Objects;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Map extends Point {
    /**
     * 类型
     */
    private String type = "map";
    /**
     * 系列名称，用于tooltip的显示，legend 的图例筛选，在 setOption 更新数据和配置项时用于指定对应的系列
     */
    private String name;
    /**
     * 地图类型
     */
    private String map;
    /**
     * 是否开启鼠标缩放和平移漫游
     */
    private Object roam;
    /**
     * 当前视角的中心点，用经纬度表示
     */
    private Object center;
    /**
     * 用于 scale 地图的长宽比
     */
    private Double aspectScale;
    /**
     * 当前视角的缩放比例
     */
    private Double zoom;
    /**
     * 滚轮缩放的极限控制，通过min, max最小和最大的缩放值，默认的缩放为1
     */
    private ScaleLimit scaleLimit;
    /**
     * 自定义地区的名称映射，如{'China' : '中国'}
     */
    private java.util.Map<String, String> nameMap;
    /**
     * 选中模式，表示是否支持多个选中，默认关闭，支持布尔值和字符串，字符串取值可选'single'表示单选，或者'multiple'表示多选
     */
    private Object selectedMode;
    /**
     * 图形上的文本标签，可用于说明图形的一些数据信息，比如值，名称等
     */
    private LabelStyle label;
    /**
     * 地图区域的多边形 图形样式
     */
    private ItemStyle itemStyle;
    /**
     * layoutCenter 和 layoutSize 提供了除了 left/right/top/bottom/width/height 之外的布局手段
     */
    private Object layoutCenter;
    /**
     * 地图的大小，见 layoutCenter。支持相对于屏幕宽高的百分比或者绝对的像素大小
     */
    private Object layoutSize;
    /**
     * 默认情况下，map series 会自己生成内部专用的 geo 组件。但是也可以用这个 geoIndex 指定一个 geo 组件
     */
    private Object geoIndex;

    /**
     * 地图数值计算方式，默认为加和，可选为：'sum'（总数） | 'average'（均值）
     */
    private String mapValueCalculation;
    /**
     * 在图例相应区域显示图例的颜色标识（系列标识的小圆点），存在 legend 组件时生效
     */
    private Boolean showLegendSymbol;
    /**
     * 列中的数据内容数组。数组项通常为具体的数据项
     */
    private List<Object> data;
    /**
     * 图表标注
     */
    private MarkPoint markPoint;
    /**
     * 图表标线
     */
    private MarkLine markLine;
    /**
     * 图表标域，常用于标记图表中某个范围的数据，例如标出某段时间投放了广告
     */
    private MarkArea markArea;
    /**
     * 图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件
     */
    private Boolean silent;

    private Tooltip tooltip;

    private Object boundingCoords;

    private Emphasis emphasis;
    /**
     * 当使用 dataset 时，seriesLayoutBy 指定了 dataset 中用行还是列对应到系列上，也就是说，系列“排布”到 dataset 的行还是列上
     */
    private String seriesLayoutBy;
    /**
     * 如果 series.data 没有指定，并且 dataset 存在，那么就会使用 dataset。datasetIndex 指定本系列使用那个 dataset
     */
    private Integer datasetIndex;

    public Integer datasetIndex() {
        return this.datasetIndex;
    }

    public Map datasetIndex(Integer datasetIndex) {
        this.datasetIndex = datasetIndex;
        return this;
    }

    public String seriesLayoutBy() {
        return this.seriesLayoutBy;
    }

    public Map seriesLayoutBy(String seriesLayoutBy) {
        this.seriesLayoutBy = seriesLayoutBy;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Map emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Object boundingCoords() {
        return boundingCoords;
    }

    public Map boundingCoords(Object boundingCoords) {
        this.boundingCoords = boundingCoords;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return tooltip;
    }

    public Map tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public String name() {
        return name;
    }

    public Map name(String name) {
        this.name = name;
        return this;
    }

    public String map() {
        return map;
    }

    public Map map(String map) {
        this.map = map;
        return this;
    }

    public Object roam() {
        return roam;
    }

    public Map roam(Object roam) {
        this.roam = roam;
        return this;
    }

    public Object center() {
        return center;
    }

    public Map center(Object value) {
       if (value instanceof Objects[]) {
            this.center = value;
        } else if (value instanceof String) {
            String[] coords = value.toString().split(",");
            this.center = new Object[]{Double.parseDouble(coords[0]), Double.parseDouble(coords[1])};
        }
        return this;
    }

    public Double aspectScale() {
        return aspectScale;
    }

    public Map aspectScale(Double aspectScale) {
        this.aspectScale = aspectScale;
        return this;
    }

    public Double zoom() {
        return zoom;
    }

    public Map zoom(Double zoom) {
        this.zoom = zoom;
        return this;
    }

    public ScaleLimit scaleLimit() {
        if (this.scaleLimit == null)
            this.scaleLimit = new ScaleLimit();
        return scaleLimit;
    }

    public Map scaleLimit(ScaleLimit scaleLimit) {
        this.scaleLimit = scaleLimit;
        return this;
    }

    public java.util.Map<String, String> nameMap() {
        if (nameMap == null)
            nameMap = new LinkedHashMap<String, String>();
        return nameMap;
    }

    public Map nameMap(String name, String value) {
        this.nameMap.put(name, value);
        return this;
    }

    public Object selectedMode() {
        return selectedMode;
    }

    public Map selectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public Map label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Map itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Object layoutCenter() {
        return layoutCenter;
    }

    public Map layoutCenter(Object value) {
        if (value instanceof Objects[]) {
            this.layoutCenter = value;
        } else if (value instanceof String) {
            String[] coords = value.toString().split(",");
            this.layoutCenter = new Object[]{Double.parseDouble(coords[0]), Double.parseDouble(coords[1])};
        } else
            this.layoutCenter = value;
        return this;
    }

    public Object layoutSize() {
        return layoutSize;
    }

    public Map layoutSize(Object layoutSize) {
        this.layoutSize = layoutSize;
        return this;
    }

    public Object geoIndex() {
        return geoIndex;
    }

    public Map geoIndex(Object geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public String mapValueCalculation() {
        return mapValueCalculation;
    }

    public Map mapValueCalculation(String mapValueCalculation) {
        this.mapValueCalculation = mapValueCalculation;
        return this;
    }

    public Boolean showLegendSymbol() {
        return showLegendSymbol;
    }

    public Map showLegendSymbol(Boolean showLegendSymbol) {
        this.showLegendSymbol = showLegendSymbol;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Map data(Object values) {
        if (data == null)
            data = new ArrayList<Object>();
        this.data.add(values);
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Map silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public MarkPoint markPoint() {
        if (markPoint == null)
            markPoint = new MarkPoint();
        return markPoint;
    }

    public Map markPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
        return this;
    }

    public MarkLine markLine() {
        if (markLine == null)
            markLine = new MarkLine();
        return markLine;
    }

    public Map markLine(MarkLine markLine) {
        this.markLine = markLine;
        return this;
    }

    public MarkArea markArea() {
        if (markArea == null)
            markArea = new MarkArea();
        return markArea;
    }

    public Map markArea(MarkArea markArea) {
        this.markArea = markArea;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public Object getRoam() {
        return roam;
    }

    public void setRoam(Object roam) {
        this.roam = roam;
    }

    public Object getCenter() {
        return center;
    }

    public void setCenter(Object center) {
        this.center = center;
    }

    public Double getAspectScale() {
        return aspectScale;
    }

    public void setAspectScale(Double aspectScale) {
        this.aspectScale = aspectScale;
    }

    public Double getZoom() {
        return zoom;
    }

    public void setZoom(Double zoom) {
        this.zoom = zoom;
    }

    public ScaleLimit getScaleLimit() {
        return scaleLimit;
    }

    public void setScaleLimit(ScaleLimit scaleLimit) {
        this.scaleLimit = scaleLimit;
    }

    public java.util.Map<String, String> getNameMap() {
        return nameMap;
    }

    public void setNameMap(java.util.Map<String, String> nameMap) {
        this.nameMap = nameMap;
    }

    public Object getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Object getLayoutCenter() {
        return layoutCenter;
    }

    public void setLayoutCenter(Object layoutCenter) {
        this.layoutCenter = layoutCenter;
    }

    public Object getLayoutSize() {
        return layoutSize;
    }

    public void setLayoutSize(Object layoutSize) {
        this.layoutSize = layoutSize;
    }

    public Object getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Object geoIndex) {
        this.geoIndex = geoIndex;
    }

    public String getMapValueCalculation() {
        return mapValueCalculation;
    }

    public void setMapValueCalculation(String mapValueCalculation) {
        this.mapValueCalculation = mapValueCalculation;
    }

    public Boolean getShowLegendSymbol() {
        return showLegendSymbol;
    }

    public void setShowLegendSymbol(Boolean showLegendSymbol) {
        this.showLegendSymbol = showLegendSymbol;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public MarkPoint getMarkPoint() {
        return markPoint;
    }

    public void setMarkPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
    }

    public MarkLine getMarkLine() {
        return markLine;
    }

    public void setMarkLine(MarkLine markLine) {
        this.markLine = markLine;
    }

    public MarkArea getMarkArea() {
        return markArea;
    }

    public void setMarkArea(MarkArea markArea) {
        this.markArea = markArea;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public Object getBoundingCoords() {
        return boundingCoords;
    }

    public void setBoundingCoords(Object boundingCoords) {
        this.boundingCoords = boundingCoords;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public String getSeriesLayoutBy() {
        return seriesLayoutBy;
    }

    public void setSeriesLayoutBy(String seriesLayoutBy) {
        this.seriesLayoutBy = seriesLayoutBy;
    }

    public Integer getDatasetIndex() {
        return datasetIndex;
    }

    public void setDatasetIndex(Integer datasetIndex) {
        this.datasetIndex = datasetIndex;
    }
}
