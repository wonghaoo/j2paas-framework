/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Force implements Serializable {
    private String initLayout;
    private Object repulsion;
    private Object gravity;
    private Object edgeLength;
    private Boolean layoutAnimation;

    public String initLayout() {
        return initLayout;
    }

    public Force initLayout(String initLayout) {
        this.initLayout = initLayout;
        return this;
    }

    public Object repulsion() {
        return repulsion;
    }

    public Force repulsion(Object repulsion) {
        this.repulsion = repulsion;
        return this;
    }

    public Object gravity() {
        return gravity;
    }

    public Force gravity(Object gravity) {
        this.gravity = gravity;
        return this;
    }

    public Object edgeLength() {
        return edgeLength;
    }

    public Force edgeLength(Object edgeLength) {
        this.edgeLength = edgeLength;
        return this;
    }

    public Boolean layoutAnimation() {
        return layoutAnimation;
    }

    public Force layoutAnimation(Boolean layoutAnimation) {
        this.layoutAnimation = layoutAnimation;
        return this;
    }

    public String getInitLayout() {
        return initLayout;
    }

    public void setInitLayout(String initLayout) {
        this.initLayout = initLayout;
    }

    public Object getRepulsion() {
        return repulsion;
    }

    public void setRepulsion(Object repulsion) {
        this.repulsion = repulsion;
    }

    public Object getGravity() {
        return gravity;
    }

    public void setGravity(Object gravity) {
        this.gravity = gravity;
    }

    public Object getEdgeLength() {
        return edgeLength;
    }

    public void setEdgeLength(Object edgeLength) {
        this.edgeLength = edgeLength;
    }

    public Boolean getLayoutAnimation() {
        return layoutAnimation;
    }

    public void setLayoutAnimation(Boolean layoutAnimation) {
        this.layoutAnimation = layoutAnimation;
    }
}
