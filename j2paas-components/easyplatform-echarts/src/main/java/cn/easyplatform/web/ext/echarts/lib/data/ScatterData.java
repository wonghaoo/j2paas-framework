/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ScatterData implements Serializable {

    private String name;
    private List<Object> value = new ArrayList<Object>();

    public ScatterData() {
    }

    public ScatterData(String name, Object[] value) {
        this.name = name;
        this.value.addAll(Arrays.asList(value));
    }

    public ScatterData(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    public ScatterData name(String namr) {
        this.name = name;
        return this;
    }

    public ScatterData value(Object value) {
        this.value.add(value);
        return this;
    }

    public ScatterData value(Object... value) {
        this.value.addAll(Arrays.asList(value));
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value.add(value);
    }
}
