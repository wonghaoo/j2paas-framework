{
    title: {
        text: 'Confidence Band',
        subtext: 'Example in MetricsGraphics.js',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            animation: false,
            label: {
                backgroundColor: '#ccc',
                borderColor: '#aaa',
                borderWidth: 1,
                shadowBlur: 0,
                shadowOffsetX: 0,
                shadowOffsetY: 0,
                color: '#222'
            }
        },
        formatter: "function(params){return params[2].name + '<br />' + params[2].value;}"
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        data: 'data',
        axisLabel: {
            formatter: "function(value,idx){var date = new Date(value);return idx === 0 ? value : [date.getMonth() + 1, date.getDate()].join('-')}"
        },
        splitLine: {
            show: false
        },
        boundaryGap: false
    },
    yAxis: {
        axisLabel: {
            formatter: "function(val){return (val - 3) * 100 + '%'}"
        },
        axisPointer: {
            label: {
                formatter: "function(params){return ((params.value - 3) * 100).toFixed(1) + '%'}"
            }
        },
        splitNumber: 3,
        splitLine: {
            show: false
        }
    },
    series: [{
        name: 'L',
        type: 'line',
        data: 'data',
        lineStyle: {
            opacity: 0
        },
        stack: 'confidence-band',
        symbol: 'none'
    }, {
        name: 'U',
        type: 'line',
        data: 'data',
        lineStyle: {
            opacity: 0
        },
        areaStyle: {
            color: '#ccc'
        },
        stack: 'confidence-band',
        symbol: 'none'
    }, {
        type: 'line',
        data: 'data',
        hoverAnimation: false,
        symbolSize: 6,
        itemStyle: {
            color: '#c23531'
        },
        showSymbol: false
    }]
}
